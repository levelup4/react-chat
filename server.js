const express = require("express");

const PORT = 4000;

// Server
const app = express();
const server = require("http").Server(app);
const io = require("socket.io")(server, {
  cors: {
    origin: "http://localhost:3000",
    methods: ["GET", "POST"]
  }
});

app.use(express.json());

// Chat
const rooms = new Map();

app.get("/rooms", (req, res) => {
  res.json(rooms);
});

app.post("/rooms", (req, res) => {
  const { roomId, userName } = req.body;
  console.log(roomId, userName);
  if (!rooms.has(roomId)) {
    rooms.set(
      roomId,
      new Map([
        ["users", new Map()],
        ["messages", []]
      ])
    );
  }
  res.json([...rooms.keys()]);
});

io.on("connection", (socket) => {
  socket.on("ROOM:JOIN", ({ roomId, userName }) => {
    socket.join(roomId);
    rooms.get(roomId).get("users").set(socket.id, userName);
    const users = [...rooms.get(roomId).get("users").values()];
    socket.to(roomId).broadcast.emit("ROOM:JOINED", users);
  });

  console.log("socket connect", socket.id);
});

// Start
server.listen(PORT, (err) => {
  if (err) {
    throw Error(err);
  }
  console.log(`Listening http://localhost:${PORT}`);
});
