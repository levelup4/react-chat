import React, { useReducer, useEffect } from "react";
import ConnectionForm from "./components/ConnectionForm";
import reducer from "./reducer";
import socket from "./socket";

function App() {
  const [state, dispatch] = useReducer(reducer, {
    joined: false,
    roomId: null,
    userName: null
  });

  const onLogin = (joinData) => {
    console.log("Login");
    dispatch({
      type: "JOINED",
      payload: joinData
    });
    socket.emit("ROOM:JOIN", joinData);
  };

  useEffect(() => {
    socket.on("ROOM:JOINED", (users) => {
      console.log("Новый пользователь", users);
    });
  }, []);

  window.socket = socket;

  return (
    <div className="container">
      {!state.joined && <ConnectionForm onLogin={onLogin} />}
    </div>
  );
}

export default App;
