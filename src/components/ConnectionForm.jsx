import React, { useState } from "react";
import axios from "axios";
import socket from "../socket";

function ConnectionForm({ onLogin }) {
  const [roomId, setRoomId] = useState("");
  const [userName, setUserName] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const onEnter = async () => {
    if (!roomId || !userName) {
      return alert("Неверные данные");
    }
    const joinData = { roomId, userName };
    setIsLoading(true);
    await axios.post("/rooms", joinData);

    onLogin(joinData);
  };

  return (
    <div className="form">
      <input
        type="text"
        placeholder="Room ID"
        className="input"
        value={roomId}
        onChange={(e) => setRoomId(e.target.value)}
      />
      <input
        type="text"
        placeholder="Ваше имя"
        className="input"
        value={userName}
        onChange={(e) => setUserName(e.target.value)}
      />
      <button className="btn-join" onClick={onEnter} disabled={isLoading}>
        {isLoading ? "Вход..." : "Войти"}
      </button>
    </div>
  );
}

export default ConnectionForm;
